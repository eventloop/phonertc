var PeerConnection;
var IceCandidate;
var SessionDescription;
var MediaStream;
var getUserMedia;
var attachMediaStream;
var reattachMediaStream;
window.webrtcPromise.then(function(res) {
  // console.debug('webrtcPromise:', res);
  PeerConnection = res.RTCPeerConnection;
  IceCandidate = res.RTCIceCandidate;
  SessionDescription = res.RTCSessionDescription;
  getUserMedia = res.getUserMedia;
  // getUserMedia = navigator.getUserMedia.bind(navigator);
  attachMediaStream = res.attachMediaStream;
  reattachMediaStream = res.reattachMediaStream;
});
// var PeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection || window.RTCPeerConnection;
// var IceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate;
// var SessionDescription = window.mozRTCSessionDescription || window.RTCSessionDescription;
// var MediaStream = window.webkitMediaStream || window.mozMediaStream || window.MediaStream;

// navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
// var getUserMedia;
// if (navigator.getUserMedia) {
//   getUserMedia = navigator.getUserMedia.bind(navigator);
// }

function LocalStream(type, subscriber) {
  var _type = type;
  var _sub = subscriber;
  var _config = {
    audio: true,
    video: type === 'video'
  };
  var _stream = null;
  var _clients = 0;

  var State = {
    NONE: 'None',
    RETRIEVING: 'Retrieving',
    STREAM: 'Stream'
  };
  var _state = State.NONE;

  var _retrieve = function() {
    getUserMedia(_config, function(stream) {
      setTimeout(function() {_onStreamSuccess(stream);});
    }, function(error) {
      setTimeout(function() {_onStreamFailure(error.name);});
    });
  };

  var _onStreamSuccess = function(stream) {
    // console.log('_onStreamSuccess!!!');
    switch (_state) {
      case State.NONE:
        stream.stop();
      break;
      case State.RETRIEVING:
        _state = State.STREAM;
        _stream = stream;
        _sub.onStreamSuccess(_type, _stream);
      break;
      case State.STREAM:
        // Impossible
      break;
    }
  };

  var _onStreamFailure = function(error) {
    switch (_state) {
      case State.NONE:
        // Do nothing
      break;
      case State.RETRIEVING:
        _sub.onStreamFailure(_type, error);
      break;
      case State.STREAM:
        // Impossible
      break;
    }
  };

  this.retrieving = function() {
    return _state == State.RETRIEVING;
  };

  this.add = function() {
    _clients++;
    switch (_state) {
      case State.NONE:
        _state = State.RETRIEVING;
        _retrieve();
      break;
      case State.RETRIEVING:
        // Just waiting
      break;
      case State.STREAM:
        setTimeout(_sub.onStreamSuccess(_type, _stream));
      break;
    }
  };

  this.rm = function() {
    _clients--;
    switch (_state) {
      case State.NONE:
        // Impossible
      break;
      case State.RETRIEVING:
        if (_clients === 0) {
          _state = State.NONE;
        }
      break;
      case State.STREAM:
        if (_clients === 0) {
          _state = State.NONE;
          // console.log('Stream stopping...');
          _stream.stop();
          _stream = null;
        }
      break;
    }
  };

}

function LocalMedia() {
  var _media = {
    audio: {
      localStream: new LocalStream('audio', this),
      subs: []
    },
    video: {
      localStream: new LocalStream('video', this),
      subs: []
    }
  };
  var _deferredType = null;

  // Firefox cannot fetch two media streams simultaneously, need to wait
  var _retrieveOrWait = function(type) {
    var otherType = type === 'audio' ? 'video' : 'audio';
    if (_media[otherType].localStream.retrieving()) {
      _deferredType = type;
    } else {
      _media[type].localStream.add();
    }
  };

  var _checkDeferred = function() {
    if (_deferredType) {
      _media[_deferredType].localStream.add();
      _deferredType = null;
    }
  };

  this.onStreamSuccess = function(type, stream) {
    var m = _media[type];
    m.subs.forEach(function(sub) {
      sub.onMediaSuccess(type, stream);
    });
    m.subs = [];
    _checkDeferred();
  };

  this.onStreamFailure = function(type, error) {
    var m = _media[type];
    m.subs.forEach(function(sub) {
      sub.onMediaFailure(type, error);
    });
    m.subs = [];
    _checkDeferred();
  };

  /*
    subscriber needs to implement callbacks:
    function onMediaSuccess(streams)
    function onMediaFailure(error)
  */
  this.subscribe = function(type, sub) {
    var m = _media[type];
    if (m.subs.indexOf(sub) < 0) {
      m.subs.push(sub);
      _retrieveOrWait(type);
    }
  };

  this.unsubscribe = function(type, sub) {
    var m = _media[type];
    var i = m.subs.indexOf(sub);
    if (i >= 0) {
      m.subs.splice(i, 1);
    }
    m.localStream.rm();
  };
}

var localMedia = new LocalMedia();

var webrtc = {
  // Adds fmtp param to specified codec in SDP.
  addCodecParam: function (sdp, codec, param) {
    var sdpLines = sdp.split('\r\n');

    // Find opus payload.
    var index = this.findLine(sdpLines, 'a=rtpmap', codec);
    var payload;
    if (index) {
      payload = this.getCodecPayloadType(sdpLines[index]);
    }

    // Find the payload in fmtp line.
    var fmtpLineIndex = this.findLine(sdpLines, 'a=fmtp:' + payload.toString());
    if (fmtpLineIndex === null) {
      return sdp;
    }

    sdpLines[fmtpLineIndex] = sdpLines[fmtpLineIndex].concat('; ', param);

    sdp = sdpLines.join('\r\n');
    return sdp;
  },

  // Find the line in sdpLines that starts with |prefix|, and, if specified,
  // contains |substr| (case-insensitive search).
  findLine: function (sdpLines, prefix, substr) {
    return this.findLineInRange(sdpLines, 0, -1, prefix, substr);
  },

  // Find the line in sdpLines[startLine...endLine - 1] that starts with |prefix|
  // and, if specified, contains |substr| (case-insensitive search).
  findLineInRange: function (sdpLines, startLine, endLine, prefix, substr) {
    var realEndLine = endLine !== -1 ? endLine : sdpLines.length;
    for (var i = startLine; i < realEndLine; ++i) {
      if (sdpLines[i].indexOf(prefix) === 0) {
        if (!substr ||
            sdpLines[i].toLowerCase().indexOf(substr.toLowerCase()) !== -1) {
          return i;
        }
      }
    }
    return null;
  },

  // Gets the codec payload type from an a=rtpmap:X line.
  getCodecPayloadType: function (sdpLine) {
    var pattern = new RegExp('a=rtpmap:(\\d+) \\w+\\/\\d+');
    var result = sdpLine.match(pattern);
    return (result && result.length === 2) ? result[1] : null;
  },

  // Returns a new m= line with the specified codec as the first one.
  setDefaultCodec: function (mLine, payload) {
    var elements = mLine.split(' ');
    var newLine = [];
    var index = 0;
    for (var i = 0; i < elements.length; i++) {
      if (index === 3) { // Format of media starts from the fourth.
        newLine[index++] = payload; // Put target payload to the first.
      }
      if (elements[i] !== payload) {
        newLine[index++] = elements[i];
      }
    }
    return newLine.join(' ');
  }
};

var streamViewNum = 0;
function StreamView(muted) {
  var _muted = muted;
  var _container = null;
  var _stream = null;
  var _streamType = null;
  var _streamId = null;

  var _remove = function(id) {
    if (id) {
      var el = document.getElementById(id);
      if (el) {
        el.src = null;
        el.parentNode.removeChild(el);
      }
    }
  };

  this.clear = function() {
    // console.log('clear: ' + _streamId);
    _remove(_streamId);
    _remove(_streamId + '_');
    _streamId = null;
  };

  function _putStreamIn(cont, streamType, stream, videoView) {
    var v = document.createElement(streamType);
    v.autoplay = true;
    v.muted = _muted;
    streamViewNum++;
    v.id = 'stream-view-' + streamViewNum;
    if (streamType == 'video') {
      // v.addEventListener('loadedmetadata', function(event) {
        v.width = cont.offsetWidth;
        v.height = cont.offsetHeight;
        cont.appendChild(v);
      // });
    } else {
      cont.appendChild(v);
    }
    if (videoView && reattachMediaStream) {
      reattachMediaStream(v, videoView);
    } else {
      attachMediaStream(v, stream);
    }
    if (videoView) {
      videoView.pause();
    }
    v.load();
    return v.id;
  }

  function _update() {
    if (_container && _stream) {
      var videoView;
      if (_streamId) {
        videoView = document.getElementById(_streamId);
      }
      var streamId = _putStreamIn(_container, _streamType, _stream, videoView);
      _remove(_streamId);
      _remove(_streamId + '_');
      // var toRemove = _streamId;
      // setTimeout(function() {_remove(toRemove), 100});
      _streamId = streamId;
    }
  }

  this.setContainer = function(container) {
    _container = container;
    _update();
  };

  this.setStream = function(streamType, stream) {
    _streamType = streamType;
    _stream = stream;
    _update();
  };

  this.getStream = function() {
    return _stream;
  };

}

function StreamViewManager() {
  var _localStreamView = null;
  var _remoteStreamContainer = null;
  var _remoteStreamViews = [];

  this.setLocalVideoView = function(view) {
    if (!_localStreamView) {
      _localStreamView = new StreamView(true);
    }
    _localStreamView.setContainer(view);
  };

  this.setRemoteVideosView = function(view) {
    _remoteStreamContainer = view;
    _remoteStreamViews.forEach(function(r) {
      r.setContainer(view);
    });
  };

  this.setLocalStream = function(type, stream) {
    window.stream = stream;
    if (!_localStreamView) {
      _localStreamView = new StreamView(true);
    }
    _localStreamView.setStream(type, stream);
  };

  this.addRemoteStream = function(type, stream) {
    var sv = new StreamView();
    _remoteStreamViews.push(sv);
    sv.setStream(type, stream);
    if (_remoteStreamContainer) {
      sv.setContainer(_remoteStreamContainer);
    }
  };

  this.delRemoteStream = function(stream) {
    for (var i = 0; i < _remoteStreamViews.length; i++) {
      var r = _remoteStreamViews[i];
      if (r.getStream() == stream) {
        r.clear();
        r.splice(i, 1);
      }
    }
  };

  this.clear = function() {
    console.log('clear!!!');
    _localStreamView.clear();
    _localStreamView = null;
    _remoteStreamViews.forEach(function(r) {
      r.clear();
    });
    _remoteStreamViews = [];
    if (_remoteStreamContainer) {
      while (_remoteStreamContainer.firstChild) {
        _remoteStreamContainer.removeChild(_remoteStreamContainer.firstChild);
      }
    }
    _remoteStreamContainer = null;
  };

}
var svm = new StreamViewManager();

function Session(sessionKey, config, sendMessageCallback) {
  var self = this;
  var State = {
    NONE: 'None',
    GETTING_MEDIA: 'GettingMedia',
    PEER: 'Peer'
  };
  self.sessionKey = sessionKey;
  self.config = config;
  self.sendMessage = sendMessageCallback;
  self.state = State.NONE;
  self.localMediaType = config.streams.video ? 'video' : 'audio';
  self.remoteMediaType = config.remoteVideoEnabled ? 'video' : 'audio';
  self.videoEabled = config.streams.video ? true : false;
  self.localStream = null;
  self.messageQueue = [];
  self.remoteSdpIsSet = false;
  self.iceCandidates = [];

  self.receiveMessage = function (message) {
    console.debug('Session state: ', self.state);
    console.debug('Session receiveMessage:', message);
    switch (self.state) {
      case State.NONE:
      break;
      case State.GETTING_MEDIA:
        self.messageQueue.push(message);
      break;
      case State.PEER:
        self.processMessage(message);
      break;
    }
  };

  self.processMessage = function(message) {
    if (message.type === 'offer') {
      self.setRemote(message);
    } else if (message.type === 'answer') {
      self.setRemote(message);
    } else if (message.type === 'candidate') {
      var candidate = new IceCandidate({
        sdpMLineIndex: parseInt(message.label),
        sdpMid: message.id,
        candidate: message.candidate
      });
      // console.debug(candidate);
      if (!self.remoteSdpIsSet) {
        self.iceCandidates.push(candidate);
      } else {
        self.addIceCandidate(candidate);
      }
    } else if (message.type === 'bye') {
      self.disconnect(false);
    }
  };

  self.addIceCandidate = function(candidate) {
    console.debug('addIceCandidate');
    self.peerConnection.addIceCandidate(candidate, function () {
      console.log('Remote candidate added successfully.');
    }, function (error) {
      // console.log(error);
      self.onError('addIceCandidate', error);
    });
  };

  self.call = function () {
    if (self.state != State.NONE) {
      console.warn('Session call while already calling');
      return;
    }
    self.state = State.GETTING_MEDIA;
    localMedia.subscribe(self.localMediaType, self);
  };

  self.onMediaSuccess = function(type, stream) {
    self.state = State.PEER;
    self.localStream = stream;
    svm.setLocalStream(type, stream);
    self.startPeerConnection();
  };

  self.onMediaFailure = function(type, error) {
    self.onError('getLocalMedia', error);
  };

  self.startPeerConnection = function() {
    var stunUrl = self.config.stun ? self.config.stun.host : 'stun:stun.l.google.com:19302';
    self.peerConnection = new PeerConnection({
      iceServers: [
        {
          url: stunUrl
        },
        {
          url: self.config.turn.host,
          username: self.config.turn.username,
          credential: self.config.turn.password
        }
      ]
    }, { optional: [ { DtlsSrtpKeyAgreement: true } ]});

    self.peerConnection.onicecandidate = self.onIceCandidate;
    self.peerConnection.onaddstream = self.onRemoteStreamAdded;

    // attach the stream to the peer connection
    self.peerConnection.addStream(self.localStream);

    // if initiator - create offer
    if (self.config.isInitiator) {
      self.sendOffer();
    }

    // Process messages that came while we were retrieving local media
    self.messageQueue.forEach(function(message) {
      self.processMessage(message);
    });
    self.messageQueue = [];
  };

  self.onIceCandidate = function (event) {
    if (event.candidate) {
      self.sendMessage({
        type: 'candidate',
        label: event.candidate.sdpMLineIndex,
        id: event.candidate.sdpMid,
        candidate: event.candidate.candidate
      });
    }
  };

  self.onRemoteStreamAdded = function (event) {
    // self.videoView = addRemoteStream(event.stream);
    // console.debug('onRemoteStreamAdded:', event);
    svm.addRemoteStream(self.remoteMediaType, event.stream);
    self.sendMessage({ type: '__answered' });
  };

  self.sendOffer = function () {
    self.peerConnection.createOffer(function (sdp) {
      if (self.state == State.NONE) return;
      self.peerConnection.setLocalDescription(sdp, function () {
        console.log('Set session description success.');
      }, function (error) {
        // console.log(error);
        self.onError('setLocalDescription', error);
      });
      sdpJson = {
        type: 'offer',
        sdp: sdp.sdp
      };
      console.debug('sendOffer: ' + JSON.stringify(sdpJson, null, 2));
      self.sendMessage(sdpJson);
    }, function (error) {
      // console.log(error);
      self.onError('createOffer', error);
    }, {
      mandatory: {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: self.videoEabled
      }
    });
  };

  self.onRemote = function() {
    if (!self.config.isInitiator) {
      self.sendAnswer();
    }
    self.remoteSdpIsSet = true;
    self.iceCandidates.forEach(function(candidate) {
      self.addIceCandidate(candidate);
    });
    self.iceCandidates = [];
  };

  self.setRemote = function (message) {
    message.sdp = webrtc.addCodecParam(message.sdp, 'opus/48000', 'stereo=1');
    var sd = new SessionDescription(message);
    this.peerConnection.setRemoteDescription(sd, function() {
      console.log('setRemote success');
      // self.onRemote();
    }, function (error) {
      // console.log(error);
      self.onError('setRemoteDescription', error);
    });
    self.onRemote();
  };

  self.sendAnswer = function () {
    self.peerConnection.createAnswer(function (sdp) {
      if (self.state == State.NONE) return;
      self.peerConnection.setLocalDescription(sdp, function () {
        // console.log('Set session description success.');
      }, function (error) {
        // console.log(error);
        self.onError('setLocalDescription', error);
      });

      sdpJson = {
        type: 'answer',
        sdp: sdp.sdp
      };
      console.debug('sendAnswer:' + JSON.stringify(sdpJson, null, 2));
      self.sendMessage(sdpJson);
    }, function (error) {
      // console.log(error);
      self.onError('createAnswer', error);
    }, {
      mandatory: {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: self.videoEnabled
      }
    });
  };

  self.renegotiate = function () {
    if (self.state == State.NONE) return;
    if (self.config.isInitiator) {
      self.sendOffer();
    } else {
      self.sendAnswer();
    }
  };

  self.disconnect = function (sendByeMessage) {
    switch (self.state) {
      case State.NONE:
      break;
      case State.PEER:
        self.state = State.NONE;
        // self.peerConnection.removeStream(self.localStream);
        self.peerConnection.close();
        localMedia.unsubscribe(self.localMediaType, self);
        self.localStream = null;
        self.messageQueue = [];
        self.remoteSdpIsSet = false;
        self.iceCandidates = [];

        self.peerConnection = null;

        if (sendByeMessage) {
          self.sendMessage({ type: 'bye' });
        }
        self.sendMessage({ type: '__disconnected' });

        svm.clear();

        // if (self.videoView) {
        //   removeRemoteStream(self.videoView);
        // }
        // onSessionDisconnect(self.sessionKey);
      break;
      case State.GETTING_MEDIA:
        self.state = State.NONE;
        localMedia.unsubscribe(self.localMediaType, self);
        self.localStream = null;
      break;
    }
  };

  self.onError = function(type, error) {
    if (self.state == State.NONE) return;
    self.sendMessage({
      type: '__error',
      content: {
        type: type,
        error: error
      }
    });
  };

  self.call();
}


var sessions = {};
var videoConfig;
var localVideoView;
var remoteVideoViews = [];
var remoteAudioViews = [];

module.exports = {
  createSessionObject: function (success, error, options) {
    var sessionKey = options[0];
    var session = new Session(sessionKey, options[1], success);

    session.sendMessage({
      type: '__set_session_key',
      sessionKey: sessionKey
    });

    sessions[sessionKey] = session;
  },
  call: function (success, error, options) {
    sessions[options[0].sessionKey].call();
  },
  receiveMessage: function (success, error, options) {
    sessions[options[0].sessionKey]
      .receiveMessage(JSON.parse(options[0].message));
  },
  renegotiate: function (success, error, options) {
    console.log('Renegotiation is currently only supported in iOS and Android.');
    // var session = sessions[options[0].sessionKey];
    // session.config = options[0].config;
    // session.createOrUpdateStream();
    // session.renegotiate();
  },
  disconnect: function (success, error, options) {
    var session = sessions[options[0].sessionKey];
    if (session) {
      session.disconnect(true);
    }
  },

  setLocalVideoView: function(success, error, options) {
    svm.setLocalVideoView(options[0]);
  },
  setRemoteVideosView: function(success, error, options) {
    svm.setRemoteVideosView(options[0]);
  },

  setVideoView: function (success, error, options) {
    videoConfig = options[0];

    if (videoConfig.containerParams.size[0] === 0 || videoConfig.containerParams.size[1] === 0) {
      return;
    }

    if (videoConfig.local) {
      if (!localVideoView) {
        localVideoView = document.createElement('video');
        localVideoView.autoplay = true;
        localVideoView.muted = true;
        localVideoView.style.position = 'absolute';
        localVideoView.style.zIndex = 999;
        localVideoView.addEventListener("loadedmetadata", scaleToFill);

        refreshLocalVideoView();
        document.body.appendChild(localVideoView);
      } else {
        refreshLocalVideoView();
        refreshVideoContainer();
      }
    }
  },
  hideVideoView: function (success, error, options) {
    localVideoView.style.display = 'none';
    remoteVideoViews.forEach(function (remoteVideoView) {
      remoteVideoView.style.display = 'none';
    });
  },
  showVideoView: function (success, error, options) {
    localVideoView.style.display = '';
    remoteVideoViews.forEach(function (remoteVideoView) {
      remoteVideoView.style.display = '';
    });
  },
  setMediaAssist: function(success, error, options) {
    var mediaAssist = options[0];
    getUserMedia = mediaAssist.get.bind(mediaAssist);
  }
};

function hasVideo(stream) {
  if (!stream.getTracks) return true; // IE
  var tracks = stream.getTracks();
  for (var i = 0; i < tracks.length; i++) {
    if (tracks[i].kind == 'video') {
      return true;
    }
  }
  return false;
}

function addRemoteStream(stream) {
  if (hasVideo(stream)) {
    var videoView = document.createElement('video');
    videoView.autoplay = true;
    videoView.addEventListener("loadedmetadata", scaleToFill);
    videoView.style.position = 'absolute';
    videoView.style.zIndex = 998;
    remoteVideoViews.push(videoView);
    document.body.appendChild(videoView);

    try {
      videoView.src = URL.createObjectURL(stream);
    } catch (e) {
      attachMediaStream(videoView, stream);
    }
    videoView.load();

    refreshVideoContainer();
    return videoView;
  } else {
    var audioView = document.createElement('audio');
    audioView.autoplay = true;
    attachMediaStream(audioView, stream);
    audioView.load();
    remoteAudioViews.push(audioView);
    document.body.appendChild(audioView);
    return audioView;
  }
}

function removeRemoteStream(view) {
  document.body.removeChild(view);
  var vi = remoteVideoViews.lastIndexOf(view);
  var ai = remoteAudioViews.lastIndexOf(view);
  if (vi >= 0) {
    remoteVideoViews.splice(vi, 1);
  } else if (ai >= 0) {
    remoteAudioViews.splice(ai, 1);
  }

  refreshVideoContainer();
}

function getCenter(videoCount, videoSize, containerSize) {
  return Math.round((containerSize - videoSize * videoCount) / 2);
}

function refreshVideoContainer() {
  if (!videoConfig) return;
  var n = remoteVideoViews.length;

  if (n === 0) {
    return;
  }

  var rows = n < 9 ? 2 : 3;
  var videosInRow = n === 2 ? 2 : Math.ceil(n/rows);

  var videoSize = videoConfig.containerParams.size[0] / videosInRow;
  var actualRows = Math.ceil(n / videosInRow);

  var y = getCenter(actualRows,
                    videoSize,
                    videoConfig.containerParams.size[1]) + videoConfig.containerParams.position[1];

  var videoViewIndex = 0;

  for (var row = 0; row < rows && videoViewIndex < n; row++) {
    var x = videoConfig.containerParams.position[0] +
      getCenter(row < rows - 1 || n % rows === 0 ? videosInRow : n - (Math.min(n, videoViewIndex + videosInRow) - 1),
                videoSize,
                videoConfig.containerParams.size[0]);

    for (var video = 0; video < videosInRow && videoViewIndex < n; video++) {
      var videoView = remoteVideoViews[videoViewIndex++];
      videoView.style.width = videoSize + 'px';
      videoView.style.height = videoSize + 'px';

      videoView.style.left = x + 'px';
      videoView.style.top = y + 'px';

      x += videoSize;
    }

    y += videoSize;
  }
}

function refreshLocalVideoView() {
  if (!videoConfig) return;
  localVideoView.style.width = videoConfig.local.size[0] + 'px';
  localVideoView.style.height = videoConfig.local.size[1] + 'px';

  localVideoView.style.left =
    (videoConfig.containerParams.position[0] + videoConfig.local.position[0]) + 'px';

  localVideoView.style.top =
    (videoConfig.containerParams.position[1] + videoConfig.local.position[1]) + 'px';
}

function scaleToFill(event) {
  var element = this;
  var targetRatio = element.offsetWidth / element.offsetHeight;
  var lastScaleType, lastAdjustmentRatio;

  function refreshTransform () {
    var widthIsLargerThanHeight = element.videoWidth > element.videoHeight;
    var actualRatio = element.videoWidth / element.videoHeight;

    var scaleType = widthIsLargerThanHeight ? 'scaleY' : 'scaleX';
    var adjustmentRatio = widthIsLargerThanHeight ?
      actualRatio / targetRatio :
      targetRatio / actualRatio ;

    if (lastScaleType !== scaleType || lastAdjustmentRatio !== adjustmentRatio) {
      var transform = scaleType + '(' + adjustmentRatio + ')';

      element.style.webkitTransform = transform;
      element.style.MozTransform = transform;
      element.style.msTransform = transform;
      element.style.OTransform = transform;
      element.style.transform = transform;

      lastScaleType = scaleType;
      lastAdjustmentRatio = adjustmentRatio;
    }

    setTimeout(refreshTransform, 100);
  }

  refreshTransform();
}

function onSessionDisconnect(sessionKey) {
  delete sessions[sessionKey];

  if (Object.keys(sessions).length === 0) {
    if (localVideoView) {
      document.body.removeChild(localVideoView);
      localVideoView = null;
    }
  }
}

require("cordova/exec/proxy").add("PhoneRTCPlugin", module.exports);
